import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CharacterButtonComponent } from './components/buttons/character-button/character-button.component';
import { MainContainerComponent } from './components/containers/main-container/main-container.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LocationButtonComponent } from './components/buttons/location-button/location-button.component';
import { LoadingOverlayComponent } from './components/loading-overlay/loading-overlay.component';
import { InterceptorService } from './services/interceptor/interceptor.service';
import { EpisodeButtonComponent } from './components/buttons/episode-button/episode-button.component';

@NgModule({
  declarations: [
    AppComponent,
    CharacterButtonComponent,
    MainContainerComponent,
    LocationButtonComponent,
    LoadingOverlayComponent,
    EpisodeButtonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass:InterceptorService, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
