<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcherInterface;

use App\Services\CharacterService;
/**
 * @Rest\Route("/characters")
 */
class CharacterController extends AbstractController
{

    /**
     * @Rest\Get("/api")
     * @QueryParam(name="page")
     * @QueryParam(name="character")
     */
    public function getCharacter(
        CharacterService $characterService, 
        ParamFetcherInterface $paramFetcher
    ) : JsonResponse {
        $params = $paramFetcher->all();
        $response = $characterService->getCharacterData($params);

        return new JsonResponse($response);
    }
}