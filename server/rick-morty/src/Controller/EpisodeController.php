<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcherInterface;

use App\Services\EpisodeService;
/**
 * @Rest\Route("/episodes")
 */
class EpisodeController extends AbstractController
{

    /**
     * @Rest\Get("/api")
     * @QueryParam(name="episode")
     * @QueryParam(name="page")
     */
    public function getLocation(
        EpisodeService $episodeService,
        ParamFetcherInterface $paramFetcher
    ) : JsonResponse {
        $params = $paramFetcher->all();
        $response = $episodeService->getEpisodeData($params);

        return new JsonResponse($response);
    }
}