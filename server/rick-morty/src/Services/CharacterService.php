<?php

namespace App\Services;
use App\Services\RickMortyApiService;

class CharacterService extends RickMortyApiService {
    const LOCATION_ENDPOINT = 'character/';

    public function getCharacterData(array $params = []) : array {
        $filter = $this->convertToQueryParams($params);
        $response = $this->getData(self::LOCATION_ENDPOINT, $filter);
        
        if (empty($response)) {
            return [];
        }

        $arrResponse = $response;
        $pageFlag = false;

        if (empty($params['character'])) {
            $arrResponse = $response['results'];
            $pageFlag = true;
        }

        foreach($arrResponse as $key => $val) {
            $episodeId = [];
            foreach($val['episode'] as $k) {
                $episodeId[] = substr($k, strpos($k, "/episode/") + 9);
            }
            $arrResponse[$key]['episode'] = $episodeId;

            if (!empty($arrResponse[$key]['location'])) {
                $a = $arrResponse[$key]['location']['url'];
                $arrResponse[$key]['location']['id'][] =  substr($a, strpos($a, "/location/") + 10);
            }

            if (!empty($arrResponse[$key]['origin'])) {
                $b = $arrResponse[$key]['origin']['url'];
                $arrResponse[$key]['origin']['id'][] =  substr($b, strpos($b, "/location/") + 10);
            }
        }

        if ($pageFlag) {
            if (!empty($response['info']['next'])) {
                $response['info']['next'] = $this->getPage($response['info']['next']);
            }
    
            if (!empty($response['info']['prev'])) {
                $response['info']['prev'] = $this->getPage($response['info']['prev']);
            }

            $response['results'] = $arrResponse;
            
            return $response;
        }
        

        return $arrResponse;
    }
}